import '../../appInfo.dart';

Map _referencedDetails = {
  // App name
  "appName": "Keyring",
  "vendorName": "Fallience",
};

Map en_us = {
  // App name and some other stuff
  "appName": _referencedDetails["appName"],
  "menu": "Menu ☰", // this isn't used yet
  
  // Page names 
  "passwordsTitle": "Passwords",
  "addAccountTitle": "Add Account",
  "generatorTitle": "Generator",
  "settingsTitle": "Settings & Info",

  // Passwords page
  "passwordsPageAddAccountButton": "Add Password +",

  // Modals
  "modalPasswordText": "Password (click to copy)",
  "modalTimestampText": "Generated at ",
  "modalCloseButtonText": "Close",
  "modalDeleteAccountButtonText": "Delete",

  // Add Account page
  "addAccountInformation": "Fill out this form to store account details and get a secure random password.",
  "addAccountUsernameFieldText": "Username/Email",
  "addAccountServiceFieldText": "Website, App, or Service name",
  "addAccountAddButtonText": "Generate & Save Password",

  // Generator
  "generatorInformation": "A secure and random password is in the box below. Click the box to copy the password and generate a new one.",

  // Encryption prompt
  "encryptionPasswordPromptHeader": "Enter your password to unlock " + _referencedDetails['appName'] + ".",
  "encryptionPasswordPromptIncorrectPasswordString": "Incorrect password.",
  "encryptionPasswordPromptTextField": "Master password",
  "enableEncryptionPasswordPromptVerifyTextField": "Verify master password",
  "encryptionPasswordPromptGo": "Unlock →",

  // enable encryption prompt
  "enableEncryptionPasswordPromptHeader": "Enter a password to lock " + _referencedDetails['appName'] + ".",
  "enableEncryptionPasswordPromptIncorrectPasswordString": "Make sure your password is at least 16 characters long.",
  "enableEncryptionPasswordPromptPasswordsDontMatchString": "The passwords you entered don't match.",
  "enableEncryptionPasswordPromptTextField": "Master password",
  "enableEncryptionPasswordPromptGo": "Lock →",

  // Settings
  "settingsLocaleHeader": "Language & Region",
  "settingsLocaleComingSoonString": "English is currently the only available language. (Localization settings delayed for β3)",

  "settingsEncryptionHeader": "Lock " + _referencedDetails['appName'],
  "settingsEncryptionDescriptionString": "Locking " + _referencedDetails['appName'] + " encrypts passwords, exports, and other sensitive information. If you set a master password, you'll enter it every time you open " + _referencedDetails['appName'] + ".",
  "settingsEncryptionWarningString": "If you forget your master password, you won't be able to view any of your passwords.",
  "settingsEncryptSetPasswordButtonString": "Lock " + _referencedDetails['appName'] + " →",
  "settingsEncryptRemovePasswordButtonString": "Unlock " + _referencedDetails['appName'] + " →",

  "settingsImportAndExportHeader": "Import & Export",
  "settingsImportAndExportString": "You can import or export all of your settings and data with the buttons below.",
  "settingsImportWarningString": "Warning: Importing will delete all of your current settings and passwords.",
  "settingsExportLinkString": "Export ↓",
  "settingsImportLinkString": "Import ↑",

  "settingsAboutHeader": "About " + _referencedDetails["vendorName"] + ' ' + _referencedDetails["appName"],
  "settingsAboutDescriptionString": _referencedDetails["appName"] + " is an open source password manager. It stores all data in your browsers storage, and can work offline.",
  "settingsAboutAppVersionString": "App Version: " + appInfo["appVersionCodename"] + ' ' + appInfo["appVersion"],
  "settingsMadeByFallienceString": "Made by Fallience with help from contributors all around the world. →",

  "settingsInstallHeader": "Installing " + _referencedDetails['appName'],
  "settingsInstallIntroductionString": "If you haven't installed the app yet, it's easy to do so.",
  "settingsInstallChromeString": "On Chrome for Android, macOS, Chrome OS, Windows, or Linux, you can install Keyring by clicking the three dots in the top right corner of the window, and selecting 'Install " + _referencedDetails["appName"] + "' or 'Add to Home Screen'.",
  "settingsInstallSafariString": "On Safari for iOS, tap the share button (box with an arrow pointing out), and select Add to Home Screen. Unfortunately, you cannot install " + _referencedDetails["appName"] + " through Safari on macOS at this time. Please use Chrome instead.",
  "settingsInstallFirefoxString": "On Firefox for Android, tap the house with a plus in the top right of the screen, and select 'Add to Home Screen'. Unfortunately, you cannot install " + _referencedDetails["appName"] + " through Firefox on any other platforms at this time. Please use Chrome or Edge instead.",
  "settingsInstallEdgeString": "Instructions for Edge are coming soon. Expect to see an app in the Microsoft Store.", // 🍺 Goodbye, EdgeHTML.
  "settingsInstallDisclaimerString": "All trademarks are property of their respective owners.",

  "settingsCreditsHeader": "Credits & Licensing",
  "settingsLicenseInfoString": "This software is licensed under the AGPL3. Click or tap here for details. →",
  "settingsSourceCodeLinkString": "Click or tap here to view this app's source code. →",
  "settingsLicensingWorkboxDisclaimerLinkString": "This software is built with the Workbox library, which is under the Apache 2.0 license. Please click or tap here for more information. →",
  "settingsLicensingInterUIDisclaimerLinkString": "The font used in this software is Inter UI, which is under the OFL 1.1. Please click or tap here for more information. →",
  "settingsLicensingEncryptDisclaimerLinkString": "This software uses the Dart package encrypt, which is under the BSD 2-clause license. Click or tap here for more information. →",
  "settingsLicensingPointyCastleDisclaimerLinkString": "This software uses the Dart package pointycastle, which is under the MPL 3.0 license. Click or tap here for more information. →",
  "settingsLicensingTwemojiDisclaimerLinkString": "This software uses Twitter (and other contributors)'s Twemoji emoji set, which is licensed under the CC-BY 4.0. Click or tap here for more information. →",
  "settingsLicensingDartDisclaimerLinkString": "The Dart programming language is used extensively in all of our software. Special thanks goes out to all contributors to Dart. Click or tap here for more information. →",
  "settingsLicensingContributorThanksDisclaimerLinkString": "Special thanks to all of our contributors. You can view some of them by clicking or tapping here. →",

  "settingsResetHeader": "Reset " + _referencedDetails['appName'],
  "settingsResetInfoString": "Reset Keyring by deleting all your settings, data, and passwords by clicking the button below.",
  "settingsResetWarningString": "This will permanently delete all data. You will not be able to recover any passwords that are deleted, unless you export them with the 'Import & Export' section above.",
  "settingsResetButtonString": "Reset ←"
};