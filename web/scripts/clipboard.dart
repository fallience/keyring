// This files point of existence is to deal with Safari.
import 'dart:html';

final bool isGarbage = window.navigator.vendor.contains('Apple Computer') || window.navigator.userAgent.contains('Edge');

void copyToClipboard(String textCopied) {
  if (isGarbage) {
    Element p = new ParagraphElement();
    p.text = textCopied;
    p.id = 'textAreaDummy';
    document.body.children.add(p);
    window.getSelection().selectAllChildren(p);
    document.execCommand('copy');
    document.body.children.remove(p);
  } else {
    window.navigator.clipboard.writeText(textCopied);
  }
}