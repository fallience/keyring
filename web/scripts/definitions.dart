import 'accountCardHandler.dart';
import 'generatorHandler.dart' as generator;

import 'package:encrypt/encrypt.dart';

import 'dart:html';
import 'dart:convert';

void getPassword() {
  void aaaaaaa() {
    initCards();
  }
  
  if (window.document.getElementById('main').dataset['password'] != null) {
    initCards();
  }

  var encryptionPrompt = document.getElementById('encryptionPasswordPrompt');
  InputElement encryptionPasswordPromptTextField = document.getElementById('encryptionPasswordPromptTextField');
  final iv = window.localStorage['iv'];
  void checkPassword() {
    final encrypter = new Encrypter(new Salsa20(encryptionPasswordPromptTextField.value, iv));
    // catch passwords that are too short
    try {
      if (encrypter.decrypt(window.localStorage['encryptionTest']) == 'Correct Horse Battery Staple') {
        window.document.getElementById('main').dataset['password'] = encryptionPasswordPromptTextField.value; 
        encryptionPrompt.classes.remove("activeEncryptionPrompt");
        aaaaaaa();
      } else {
        window.document.getElementById('encryptionPasswordPromptIncorrectPasswordString').classes.add('stringVisible');
      }
    } catch (e) {
      window.document.getElementById('encryptionPasswordPromptIncorrectPasswordString').classes.add('stringVisible');
    }
  }

  encryptionPrompt.classes.add("activeEncryptionPrompt");
  document.getElementById('encryptionPasswordPromptGo').onClick.listen(
    (event) => checkPassword()
  );
}

Map Account(String service, String username, String password) {
  return {
    "id": generator.secureStringDart(50, r"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#_=+$^"),
    "service": service,
    "username": username,
    "password": password,
    "accountCreationTime": new DateTime.now().millisecondsSinceEpoch
  };
}

void writeEncryptionTest() async {
  final String key = window.document.getElementById('main').dataset['password']; // password must be at least 16 characters
  final iv = window.localStorage['iv'];
  final encrypter = await new Encrypter(new Salsa20(key, iv));
  window.localStorage['encryptionTest'] = await encrypter.encrypt('Correct Horse Battery Staple');
}

Future<List> readAccountList() async {
  final String key = window.document.getElementById('main').dataset['password']; // password must be at least 16 characters
  final iv = window.localStorage['iv'];
  if (window.localStorage['encrypted'] == 'false') {
    return await json.decode(window.localStorage['accountList']);
  } else {
    final encrypter = await new Encrypter(new Salsa20(key, iv));
    return await json.decode(await encrypter.decrypt(await window.localStorage['accountList']));
  }
}

void writeAccountList(List list) {
  final String key = window.document.getElementById('main').dataset['password']; // password must be at least 16 characters
  final iv = window.localStorage['iv'];
  if (window.localStorage['encrypted'] == 'false') {
    window.localStorage['accountList'] = json.encode(list);
  } else {
    final encrypter = new Encrypter(new Salsa20(key, iv));
    window.localStorage['accountList'] = encrypter.encrypt(json.encode(list));
  }
}

void accountsListPush(Map account) async {
  List currentAccountsList = await readAccountList();
  await currentAccountsList.add(json.encode(account));
  writeAccountList(currentAccountsList);
}

void accountsListRemove(Map account) async {
  List currentAccountsList = await readAccountList();
  await currentAccountsList.remove(json.encode(account));
  writeAccountList(currentAccountsList);
}

void storeAccount(String service, String username, String password) {
  var account = Account(service, username, password);
  accountsListPush(account);
  initCards();
}

void removeAccount(Map account) {
  accountsListRemove(account);
  initCards();
}