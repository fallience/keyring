import 'dart:html';
import 'definitions.dart';

void openModal(Map accountObject) {
  print(accountObject['id']);
  document.getElementById("modalHeader").text = accountObject['username'];
  document.getElementById("modalHeaderService").text = accountObject['service'];
  document.getElementById("passwordField").text = accountObject['password'];

  document.getElementById('blocker').classes.add('fadeIn');
  document.getElementById('passwordModal').classes.add('slideIn');

  document.getElementById('modalDeleteAccountButton').onClick.listen(
    (event) => handleDeleteButton(accountObject)
  );
}

void handleDeleteButton(Map accountObject) {
  document.getElementById('modalDeleteAccountButton').remove();
  var newDeleteButton = new InputElement();
  newDeleteButton.id = 'modalDeleteAccountButton';
  newDeleteButton.classes.add('local_modalDeleteAccountButtonText');
  newDeleteButton.classes.add('modalButton');  
  newDeleteButton.type = 'button';
  document.getElementById('passwordModal').children.add(newDeleteButton);
  
  removeAccount(accountObject);
  closeModal();
}

void closeModal() {
  document.getElementById('blocker').classes.remove('fadeIn');
  document.getElementById('passwordModal').classes.remove('slideIn');
}