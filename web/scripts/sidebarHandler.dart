import 'dart:html';
import 'activeHandler.dart';

void initSidebar() {
  // TODO: automate most of this

  // add new buttons here
  document.getElementById('passwordsPageButton').onClick.listen(
    (event) => setActive('passwordsPage')
  );
  document.getElementById('addPageButton').onClick.listen(
    (event) => setActive('addPage')
  );
  document.getElementById('generatorPageButton').onClick.listen(
    (event) => setActive('generatorPage')
  );
  document.getElementById('aboutPageButton').onClick.listen(
    (event) => setActive('aboutPage')
  );
}