import 'dart:html';

void setActive(String pageName) {
  if (document.getElementById(pageName) != null){
    if (querySelector('.pageActive') != null) {
      querySelector('.pageActive').classes.remove('pageActive');
    }
    if (querySelector('.sidebarButtonActive') != null) {
      querySelector('.sidebarButtonActive').classes.remove('sidebarButtonActive');
    }
    document.getElementById(pageName).classes.add('pageActive');
    document.getElementById(pageName + 'Button').classes.add('sidebarButtonActive');
  }
}