Simple password manager written in Dart and delivered as a PWA. 

## Contributing
You'll need Dart and a modern web browser.

Once you have those, just run these commands from the project root:

```
$ pub global activate webdev
$ pub get
$ webdev serve
```

and you can start editing. Open the URL given by `webdev serve` in your favorite browser, and refresh twice when you make a change.

No matter the change though, make sure to change the version number in service-worker.js.

**NOTE**: Safari do not play nice with `webdev serve` - use `webdev build` then host a server out of the build directory instead.

## Licensing
All code and assets are governed by the AGPL-3.0 license (see LICENSE.md), with the exception of files in `web/workbox` and `build/workbox`, which are copyright Google 2018 and are under the Apache 2.0 license (see `web/workbox/LICENSE`), the files in `build/Inter-UI` and `web/Inter-UI`, which are copyright the Inter UI authors and are under the Open Font License version 1.1 (see `web/Inter-UI/LICENSE.txt`), the files `build/scripts/download.js` and `web/scripts/download.js`, which are copyright dandavis (see http://danml.com/download.html).

Copyright the Fallience Keyring authors 2018
